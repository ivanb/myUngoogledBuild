# myUngoogledBuild

ungoogled-chromium amd64

**Windows**

unpack into a desired folder, run chrome.exe

**Debian**

sudo apt install /fullpath/to/package/file.deb

**source code**

https://github.com/ungoogled-software/ungoogled-chromium-windows.git

https://github.com/ungoogled-software/ungoogled-chromium-debian.git
